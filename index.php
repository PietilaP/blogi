<?php 
include_once 'inc/top.php';
?>
<div class="container">
    <div><br>
    <?php
        try {
            // Muodostetaan suoritettava sql-lause.
            $sql = "SELECT *,kirjoitus.id as id FROM kirjoitus INNER JOIN kayttaja ON kirjoitus.kayttaja_id = kayttaja.id" .
                    " ORDER BY paivays desc";
           
            // Suoritetaan kysely tietokantaan.
            $kysely = $tietokanta->query($sql);
            
            if ($kysely) {
            
                while ($tietue = $kysely->fetch()) {
                    print '<div class="kirjoitus">';
                    print  '<p class="p"> ' . date("d.m.Y H.i", strtotime($tietue['paivays'])) . ' by ' . $tietue['tunnus'] . '</p>';
                    print '<p><a href="blogi.php?id=' . $tietue['id'] . '">' . $tietue['otsikko'] . '</a>' . ' ';
                    print '<a href="poista.php?id=' . $tietue['id']. '" onclick="return confirm(\'Jatketaanko?\');">'
                            . '<span class ="glyphicon glyphicon-trash"></a></p>';
                    print '</div>';
                }
            } else {
                print '<p>';
                print_r($tietokanta->errorInfo());
                print '</p>';
            }
            
        } catch (PDOException $pdoex) {
            print '<p>Tietokannan avaus epäonnistui.' . $pdoex->getMessage(). '</p>';
        }
    ?>

    </div>
</div>
<?php include_once 'inc/bottom.php';?>