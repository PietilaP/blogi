<?php include_once 'inc/top.php';
$viesti = "";
$tietokanta = new PDO('mysql:host=localhost;dbname=Blogi;charset=utf8','root','');
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if($tietokanta!=null) {
        try {
            $tunnus = filter_input(INPUT_POST, 'tunnus', FILTER_SANITIZE_STRING);
            $salasana = md5(filter_input(INPUT_POST, 'salasana', FILTER_SANITIZE_STRING));

            $sql="SELECT * FROM kayttaja where tunnus='$tunnus' AND salasana='$salasana'";

            $kysely = $tietokanta->query($sql);

            if ($kysely->rowCount()===1){
                $tietue = $kysely->fetch();
                $_SESSION['login'] = true;
                $_SESSION['kayttaja_id'] = $tietue['id'];
                header('Location: index.php');
            }
            else {
                $viesti = "Väärä tunnus tai salasana!";
            }
        } catch (PDOException $pdoex) {
            print "Käyttajan tietojen hakeminen epäonnistui." . $pdoex->getMessage();
        }
    }
}
?>

<div class="container">

    <div><br>
        <form method="post" action="<?php print ($_SERVER['PHP_SELF']); ?>">
            <div class="form-group">
                <label for="tunnus">Tunnus:</label><br>
                <input name="tunnus" type="text" class="form-control"><br>
            </div>
            <div class="form-group">
                <label for="salasana">Salasana:</label><br>
                <input name="salasana" type="text" class="form-control">
             </div>    
            <button class="btn btn-default" type="submit">Kirjaudu</button>

        </form><br>
        
        <h3 style="color:maroon"><?php echo $viesti ?></h3>
      </div>

    </div><!-- /.container -->

<?php include_once 'inc/bottom.php';?>