<?php include_once 'inc/top.php';?>

<?php
    session_destroy();
    header('Location: index.php');
?>

<?php include_once 'inc/bottom.php';?>