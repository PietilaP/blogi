<?php 
include_once 'inc/top.php';
?>
<div class="container"><br>
    <?php
    $id = filter_input(INPUT_GET,'id',FILTER_SANITIZE_NUMBER_INT);
    $kirjoitus_id = filter_input(INPUT_GET,'kirjoitus_id',FILTER_SANITIZE_NUMBER_INT);

    try {
        // Muodostetaan parametroitu sql-kysely tiedon poistamista varten.
        $kysely = $tietokanta->prepare("DELETE FROM kommentti WHERE id=:id");

        $kysely->bindValue(':id',$id,PDO::PARAM_INT);

        // Suoritetaan kysely ja tarkastetaan samalla mahdollinen virhe.
        if ($kysely->execute()) {
            print('<p>Kommentti poistettu</p>');
        }
        else {
            print '<p>';
            print_r($tietokanta->errorInfo());
            print '</p>';
        }
        print("<a href='blogi.php?id=" . $kirjoitus_id . "'>Takaisin kirjoitukseen</a>");


    } catch (PDOException $pdoex) {
        print '<p>Tietokannan avaus epäonnistui.' . $pdoex->getMessage(). '</p>';
    }
    ?>
</div>
<?php 
include_once 'inc/bottom.php';
?>