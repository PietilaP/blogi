<?php include_once 'inc/top.php';?>

<?php
$viesti = "";
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            try {
                $otsikko = filter_input(INPUT_POST, 'otsikko', FILTER_SANITIZE_STRING);
                $teksti = filter_input(INPUT_POST, 'teksti', FILTER_SANITIZE_STRING);
                $kayttaja_id = $_SESSION['kayttaja_id'];

                $kysely = $tietokanta->prepare("INSERT INTO kirjoitus(otsikko, teksti, kayttaja_id) VALUES (:otsikko,:teksti,:kayttaja_id)");

                $kysely->bindValue(':otsikko', $otsikko,PDO::PARAM_STR);
                $kysely->bindValue(':teksti', $teksti,PDO::PARAM_STR);
                $kysely->bindValue(':kayttaja_id', $kayttaja_id,PDO::PARAM_INT);

                if ($kysely->execute()) {
                    $viesti='<p>Kirjoitus lisätty!</p>';
                }
                else {
                    print "<p>";
                    print_r($tietokanta->errorInfo());
                    print "</p>";
                }
                $viesti .= "<a href='index.php'>Etusivulle</a>";

            } catch (PDOException $pdoex) {
                print '<p>Tietokannan avaus epäonnistui.' . $pdoex->getMessage(). '</p>';
            }
        }

?>

<div class="container">

    <div><br>
          <?php echo $viesti ?>
          
        <h1>Lisää kirjoitus</h1>
        <form method="post" action="<?php print ($_SERVER['PHP_SELF']); ?>">
            <div class="form-group">
                <label for="otsikko">Otsikko</label><br>
                <input name="otsikko" type="text" class="form-control" placeholder="Otsikko tähän" required><br>
            </div>
            <div class="form-group">
                <label for="teksti">Teksti</label><br>
                <input name="teksti" type="text" class="form-control" placeholder="Teksti tähän" required><br>
             </div>    
            <button class="btn btn-primary" type="submit">Tallenna</button><button type="reset" class="btn btn-default">Peruuta</button>

        </form>
      </div>

    </div><!-- /.container -->

<?php include_once 'inc/bottom.php';?>