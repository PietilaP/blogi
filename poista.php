<?php include_once 'inc/top.php';?>
    <div class="container">
        <div><br>
        <?php
        $id = filter_input(INPUT_GET,'id',FILTER_SANITIZE_NUMBER_INT);

        try {

            // Muodostetaan parametroitu sql-kysely tiedon poistamista varten.
            $kysely = $tietokanta->prepare("DELETE FROM kommentti WHERE kirjoitus_id=:id; DELETE FROM kirjoitus WHERE id=:id");

            $kysely->bindValue(':id',$id,PDO::PARAM_INT);

            // Suoritetaan kysely ja tarkastetaan samalla mahdollinen virhe.
            if ($kysely->execute()) {
                print('<p>Kirjoitus poistettu</p>');
            }
            else {
                print '<p>';
                print_r($tietokanta->errorInfo());
                print '</p>';
            }
            print("<a href='index.php'>Takaisin etusivulle</a>");


        } catch (PDOException $pdoex) {
            print '<p>Tietokannan avaus epäonnistui.' . $pdoex->getMessage(). '</p>';
        }
        ?>
      </div>
    </div>
<?php include_once 'inc/bottom.php';?>