<?php
session_start();
session_regenerate_id();

$tietokanta = new PDO('mysql:host=localhost;dbname=Blogi;charset=utf8','root','');

$tietokanta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Blogi">
    <meta name="author" content="Pauli Pietilä">
    <link rel="icon" href="../../favicon.ico">

    <title>Blogi</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">

  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Blogi</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="index.php">Etusivulle</a></li>
            <?php if (isset($_SESSION['kayttaja_id'])) {?>
            <li><a href="kirjoitus.php">Lisää kirjoitus</a></li>
            <?php }?>
            <?php if (isset($_SESSION['kayttaja_id'])) {?>
            <li><?php print '<a href="logout.php" onclick="return confirm(\'Kirjaudutaanko varmasti ulos?\');">Kirjaudu ulos</a>'; ?></li>
            <?php } else { ?>
            <li><a href="kirjaudu.php">Kirjaudu sisään</a></li>
            <?php }?>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>