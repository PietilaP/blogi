<?php 
include_once 'inc/top.php';
?>
<div class="container"><br>
    <?php
    if ($_SERVER['REQUEST_METHOD']=="GET") {
        $id = filter_input(INPUT_GET,'id',FILTER_SANITIZE_NUMBER_INT);

        try {
            // Muodostetaan parametroitu sql-kysely tiedon hakemista varten.
            $kysely = $tietokanta->prepare("SELECT * FROM kirjoitus JOIN kayttaja ON kayttaja.id = kirjoitus.kayttaja_id WHERE kirjoitus.id=:id");
            $kysely->bindValue(':id',$id,PDO::PARAM_INT);

            // Suoritetaan kysely ja tarkastetaan samalla mahdollinen virhe.
            if ($kysely->execute()) {
                $tietue = $kysely->fetch();
                print "<a href='index.php'>Takaisin etusivulle</a>";
                print "<h3>" . $tietue['otsikko'] . "</h3>";
                print "</p>" . date("d.m.Y H.i", strtotime($tietue['paivays'])) . ' by ' . $tietue['tunnus'] . '</p>';
                print "</p>" . $tietue['teksti'] . '</p><br>';
                print "<h4>Kommentit</h4>";
            }
            else {
                print '<p>';
                print_r($tietokanta->errorInfo());
                print '</p>';
            }
        } catch (PDOException $pdoex) {
            print '<p>Tietokannan avaus epäonnistui.' . $pdoex->getMessage(). '</p>';
        }
    }
    else if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        try {
            $kirjoitus_id = filter_input(INPUT_POST, 'kirjoitus_id', FILTER_SANITIZE_NUMBER_INT);
            $teksti = filter_input(INPUT_POST, 'kommentti', FILTER_SANITIZE_STRING);


            $kysely = $tietokanta->prepare("INSERT INTO kommentti(teksti, kirjoitus_id, kayttaja_id) VALUES (:teksti,:kirjoitus_id,:kayttaja_id)");

            $kysely->bindValue(':teksti', $teksti,PDO::PARAM_STR);
            $kysely->bindValue(':kirjoitus_id', $kirjoitus_id,PDO::PARAM_INT);
            $kysely->bindValue(':kayttaja_id', $_SESSION['kayttaja_id'],PDO::PARAM_INT);

            $kysely->execute();
            header("Location: blogi.php?id=$kirjoitus_id");
            exit;

        } catch (PDOException $pdoex) {
            print '<p>Tietokannan avaus epäonnistui.' . $pdoex->getMessage(). '</p>';
        }
    }
    ?>
    
    <?php if (isset($_SESSION['kayttaja_id'])) {?>
    <form id="lisaa_kommentti" method="post" action="<?php print($_SERVER['PHP_SELF']);?>">
        <div class="form-group">
            <input type="hidden" name="kirjoitus_id" value="<?php print $id;?>">
            <textarea style="width:250px; height:100px;" name="kommentti" id="kommentti" required></textarea>
        </div>
        <button class="btn btn-primary" type="submit">Lähetä</button><button type="reset" class="btn btn-default">Peruuta</button>
    </form>
    <?php } else {
        print "<p>Kirjaudu sisään lisätäksesi kommentti!</p>";
    }
?>

    <?php
    try {
        $kysely = $tietokanta->prepare("SELECT *, kommentti.id as id FROM kommentti JOIN kayttaja ON kommentti.kayttaja_id = kayttaja.id WHERE kommentti.kirjoitus_id = :id ORDER BY paivays desc");
        $kysely->bindValue(':id',$id,PDO::PARAM_INT);

        // Suoritetaan kysely tietokantaan.
        $kysely->execute();

        if ($kysely) {
            print "<ul id='kommentit'>";
            while ($tietue = $kysely->fetch()) {
                print "<li>" . $tietue['teksti'] . " " 
                        . date("d.m.Y H.i", strtotime($tietue['paivays'])) 
                        . ' by ' . $tietue['tunnus'] 
                        . '<a href="poista_kommentti.php?id=' . $tietue['id'] . '&kirjoitus_id=' . $tietue['kirjoitus_id']
                        . '" onclick="return confirm(\'Jatketaanko?\');">'
                        . '<span class ="glyphicon glyphicon-trash"></a></p></li>';;
            }
            print "</ul>";
        } else {
            print '<p>';
            print_r($tietokanta->errorInfo());
            print '</p>';
        }

    } catch (PDOException $pdoex) {
        print '<p>Tietokannan avaus epäonnistui.' . $pdoex->getMessage(). '</p>';
    }
        
        
?>
        
    </div>
<?php include_once 'inc/bottom.php';?>